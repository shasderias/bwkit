package main

import (
	"fmt"
	"os"
	"strings"
)

type bashShell struct{}

func (sh bashShell) render(env envMap) {
	for k, v := range env {
		fmt.Fprintf(os.Stdout, "export '%s'='%s'\n", sh.escapeStr(k), sh.escapeStr(v))
	}
}

func (sh bashShell) escapeStr(s string) string {
	return strings.Replace(s, `'`, `'\''`, -1)
}
