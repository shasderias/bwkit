package main

import (
	"fmt"
	"os"
	"strings"
)

type fishShell struct{}

func (sh fishShell) render(env envMap) {
	for k, v := range env {
		fmt.Fprintf(os.Stdout, "set -xg '%s' '%s'\n", sh.escapeStr(k), sh.escapeStr(v))
	}
}

func (sh fishShell) escapeStr(s string) string {
	s = strings.Replace(s, `\`, `\\`, -1)
	s = strings.Replace(s, `'`, `\'`, -1)
	return s
}
