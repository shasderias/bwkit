package main

import (
	"fmt"
	"os"
)

type batchShell struct{}

func (sh batchShell) render(env envMap) {
	for k, v := range env {
		fmt.Fprintf(os.Stdout, "set %s=%s\n", sh.escapeStr(k), sh.escapeStr(v))
	}
}

func (sh batchShell) escapeStr(s string) string {
	// TODO: https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/set_1
	// consider restricting characters <, >, |, &, ^
	return s
}
