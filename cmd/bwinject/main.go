package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"

	"gitlab.com/shasderias/bwkit/bw"
)

var (
	evRegex   = regexp.MustCompile(`\$\{` + bw.EnvVarMagicStr + `_(\w+)\}`)
	dataRegex = regexp.MustCompile(`\$\{` + bw.DataMagicStr + `_(\w+)\}`)
)

func printUsage(fs *flag.FlagSet) {
	const usage = `bwinject FILE [QUERY]
	bwinject searches through FILE for all sub-strings that match the regex
	\$\{([_a-zA-Z]+)\}
and replaces the first group of each match (i.e. the part between curly braces)
with values from the Bitwarden vault.`

	fmt.Fprintf(os.Stderr, "%s\n", usage)
	fs.PrintDefaults()
}

type config struct {
	skipSync    bool
	writeResult bool

	filename string
	query    string
}

func getConfig() config {
	conf := config{}

	fs := flag.NewFlagSet("", flag.ExitOnError)

	fs.BoolVar(&conf.skipSync, "skip-sync", false, "skip Bitwarden vault sync")
	fs.BoolVar(&conf.writeResult, "write", false, "write result to file instead of stdout")

	fs.Parse(os.Args[1:])

	switch fs.NArg() {
	case 0:
		printUsage(fs)
		os.Exit(1)
	case 1:
		conf.filename = fs.Arg(0)
	case 2:
		conf.filename = fs.Arg(0)
		conf.query = fs.Arg(1)
	}

	return conf
}

func main() {
	conf := getConfig()

	ev := make(map[string]string, 0)
	data := make(map[string]string, 0)

	in := readFile(conf)
	parseEVPlaceholders(conf, in, ev)
	parseDataPlaceholders(conf, in, data)

	checkVaultLock()
	doVaultSync(conf)

	fillEVFromVault(conf, ev)
	fillDataFromVault(conf, data)

	out := replacePlaceholders(conf, in, ev, data)
	writeResult(conf, out)
}

func readFile(conf config) []byte {
	file, err := ioutil.ReadFile(conf.filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error reading %s: %v", conf.filename, err)
		os.Exit(1)
	}
	return file
}

func parseEVPlaceholders(conf config, b []byte, ev map[string]string) {
	matches := evRegex.FindAllSubmatch(b, -1)

	for _, match := range matches {
		ev[string(match[1])] = ""
	}
}

func parseDataPlaceholders(conf config, b []byte, data map[string]string) {
	matches := dataRegex.FindAllSubmatch(b, -1)

	for _, match := range matches {
		data[string(match[1])] = ""
	}
}

func checkVaultLock() {
	if bw.Locked() {
		fmt.Fprintf(os.Stderr, "vault locked, execute bw login and/or bw unlock\n")
		os.Exit(1)
	}
}

func doVaultSync(conf config) {
	if conf.skipSync {
		fmt.Fprintf(os.Stderr, "skipping vault sync\n")
	} else {
		fmt.Fprintf(os.Stderr, "syncing vault ... ")
		if err := bw.Sync(); err != nil {
			fmt.Fprintf(os.Stderr, "error syncing bw vault\n")
			fmt.Fprintf(os.Stderr, "%v", err)
			os.Exit(1)
		}
		fmt.Fprintf(os.Stderr, "done\n")
	}
}

func fillEVFromVault(conf config, ev map[string]string) {
	vaultEVs, err := bw.GetEnvVars(conf.query)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error retrieving env vars from vault for '%s': %v\n", conf.query, err)
		os.Exit(1)
	}

	for k := range ev {
		ev[k] = vaultEVs[k]
	}
}

func fillDataFromVault(conf config, data map[string]string) {
	for k := range data {
		val, err := bw.GetData(k)
		if err != nil {
			fmt.Fprintf(os.Stderr, "vault does not contain item for data '%s'\n", k)
			os.Exit(1)
		}
		data[k] = val
	}
}

func replacePlaceholders(conf config, b []byte, ev, data map[string]string) []byte {
	b = evRegex.ReplaceAllFunc(b, func(m []byte) []byte {
		const prefixLen = len("${" + bw.EnvVarMagicStr + "_")
		const suffixLen = len("}")

		val := ev[string(m[prefixLen:len(m)-suffixLen])]
		if val == "" {
			return m
		}
		escapedVal, _ := json.Marshal(val)
		return []byte(escapedVal)
	})

	b = dataRegex.ReplaceAllFunc(b, func(m []byte) []byte {
		const prefixLen = len("${" + bw.DataMagicStr + "_")
		const suffixLen = len("}")

		val := data[string(m[prefixLen:len(m)-suffixLen])]
		if val == "" {
			return m
		}
		escapedVal, _ := json.Marshal(val)
		return []byte(escapedVal)
	})

	return b
}

func writeResult(conf config, b []byte) {
	if conf.writeResult {
		if err := ioutil.WriteFile(conf.filename, b, 0300); err != nil {
			fmt.Fprintf(os.Stderr, "error writing output to %s: %v", conf.filename, err)
			os.Exit(1)
		}
		return
	}

	fmt.Fprintf(os.Stdout, "%s", b)
}
