package bw

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/gofrs/uuid"
)

const (
	bwSessionEnvKey = "BW_SESSION"
	bwExec          = "bw"
	bwCmdSync       = "sync"
)

var (
	bwCmdListItems = []string{"list", "items", "--search"}
)

const (
	EnvVarMagicStr = "bwenv"
	DataMagicStr   = "bwdata"
)

type Item struct {
	Object         string        `json:"object"`
	ID             uuid.UUID     `json:"id"`
	OrganizationID uuid.NullUUID `json:"organizationId"`
	FolderID       uuid.NullUUID `json:"folderId"`
	Type           ItemType      `json:"type"`
	Name           string        `json:"name"`
	Notes          string        `json:"notes"`
	Favorite       bool          `json:"favorite"`
	Fields         []Field       `json:"fields"`
	SecureNote     SecureNote    `json:"secureNote"`
	CollectionIDs  []uuid.UUID   `json:"collectionIds"`
	RevisionDate   time.Time     `json:"revisionDate"`
}

type Field struct {
	Name  string    `json:"name"`
	Value string    `json:"value"`
	Type  FieldType `json:"type"`
}

type SecureNote struct {
	SecureNoteType `json:"secureNoteType"`
}

type ItemType int

const (
	ItemTypeLogin      = 1
	ItemTypeSecureNote = 2
	ItemTypeCard       = 3
	ItemTypeIdentity   = 4
)

type FieldType int

const (
	FieldTypeText    = 0
	FieldTypeHidden  = 1
	FieldTypeBoolean = 2
)

type SecureNoteType int

const (
	SecureNoteTypeGeneric = 0
)

func Locked() bool {
	env := os.Getenv(bwSessionEnvKey)
	return env == ""
}

func Sync() error {
	syncCmd := exec.Command(bwExec, bwCmdSync)

	return syncCmd.Run()
}

func SearchItems(query string) ([]Item, error) {
	q := EnvVarMagicStr
	if query != "" {
		q += "-" + query
	}

	args := []string{}
	args = append(args, bwCmdListItems...)
	args = append(args, q)

	searchCmd := exec.Command(bwExec, args...)

	itemsJSON, err := searchCmd.Output()

	if err != nil {
		if exitErr, ok := err.(*exec.ExitError); ok {
			return nil, errors.New(string(exitErr.Stderr))
		}
		return nil, err
	}

	var items []Item

	if err := json.Unmarshal(itemsJSON, &items); err != nil {
		return nil, err
	}

	return items, nil
}

func GetData(query string) (string, error) {
	q := DataMagicStr + "_" + query

	items, err := listCmd(q)
	if err != nil {
		return "", err
	}

	if l := len(items); l != 1 {
		return "", fmt.Errorf("query for GetData should return exactly 1 item, got %d instead", l)
	}

	if strings.Contains(query, "base64") {
		return base64.StdEncoding.EncodeToString([]byte(items[0].Notes)), nil
	}
	return items[0].Notes, nil
}

func GetEnvVars(query string) (map[string]string, error) {
	var q string
	if query == "" {
		q = EnvVarMagicStr
	} else {
		q = EnvVarMagicStr + "-" + query
	}

	items, err := listCmd(q)
	if err != nil {
		return nil, err
	}

	ev := make(map[string]string, 0)

	for _, item := range items {
		if item.Type != ItemTypeSecureNote {
			continue
		}
		for _, field := range item.Fields {
			if field.Type != FieldTypeText {
				continue
			}

			ev[field.Name] = field.Value
		}
	}

	return ev, nil
}

func listCmd(query string) ([]Item, error) {
	args := []string{}
	args = append(args, bwCmdListItems...)
	args = append(args, query)

	listCmd := exec.Command(bwExec, args...)

	itemsJSON, err := listCmd.Output()
	if err != nil {
		return nil, fmt.Errorf("error executing bw list command: %v", err)
	}

	var items []Item
	if err := json.Unmarshal(itemsJSON, &items); err != nil {
		return nil, err
	}

	return items, nil
}
